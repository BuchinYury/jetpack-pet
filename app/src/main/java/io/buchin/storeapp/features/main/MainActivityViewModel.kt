package io.buchin.storeapp.features.main

import androidx.lifecycle.MutableLiveData

class MainActivityViewModel(
  private val initialState: MainActivityViewState = MainActivityViewState()
) {
  val viewState = MutableLiveData<MainActivityViewState>().apply {
    postValue(initialState)
  }

  fun storeFrontTabClicked() {
    val currentViewState = viewState.value
    if (currentViewState?.storeFrontTabVisible == false)
      viewState.postValue(currentViewState.copy(currentTab = MainActivityTab.StoreFrontTab))
  }

  fun backEndTabClicked() {
    val currentViewState = viewState.value
    if (currentViewState?.backEndTabVisible == false)
      viewState.postValue(currentViewState.copy(currentTab = MainActivityTab.BackEndTab))
  }
}

data class MainActivityViewState(
  private val currentTab: MainActivityTab = MainActivityTab.StoreFrontTab
) {
  val storeFrontTabVisible = currentTab === MainActivityTab.StoreFrontTab
  val backEndTabVisible = currentTab === MainActivityTab.BackEndTab
}

sealed class MainActivityTab {
  object StoreFrontTab : MainActivityTab()
  object BackEndTab : MainActivityTab()
}