package io.buchin.storeapp.features.backend

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import io.buchin.storeapp.R

class BackEndFragment : Fragment() {
  // region lifecycle
  override fun onCreateView(
      inflater: LayoutInflater,
      container: ViewGroup?,
      savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.fragment_back_end, container, false)
  // endregion
}