package io.buchin.storeapp.features.storefront

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import io.buchin.storeapp.R
import io.buchin.storeapp.databinding.FragmentStoreFrontBinding


class StoreFrontFragment : Fragment() {

  private val viewModel = StoreFrontViewModel()
  private lateinit var binding: FragmentStoreFrontBinding

  // region lifecycle
  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    binding = DataBindingUtil.inflate(
      inflater, R.layout.fragment_store_front, container, false
    )

    binding.viewModel = viewModel

    return binding.root
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    viewModel.viewState.observe(this, Observer<StoreFrontViewState>{
      binding.viewState = it
    })
  }
  // endregion
}