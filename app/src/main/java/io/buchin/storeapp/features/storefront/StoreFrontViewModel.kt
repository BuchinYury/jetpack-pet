package io.buchin.storeapp.features.storefront

import androidx.lifecycle.MutableLiveData
import java.util.*


class StoreFrontViewModel(
  private val initialState: StoreFrontViewState = StoreFrontViewState()
) {
  val viewState = MutableLiveData<StoreFrontViewState>().apply {
    postValue(initialState)
  }

  fun newViewState() {
    viewState.postValue(StoreFrontViewState())
  }
}

class StoreFrontViewState(
  val text: String = UUID.randomUUID().toString()
)