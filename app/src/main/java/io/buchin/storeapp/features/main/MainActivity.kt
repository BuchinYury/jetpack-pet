package io.buchin.storeapp.features.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import io.buchin.storeapp.R
import io.buchin.storeapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
  private val viewModel = MainActivityViewModel()
  private lateinit var binding: ActivityMainBinding

  // region lifecycle
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

    viewModel.viewState.observe(this, Observer<MainActivityViewState> {
      binding.viewState = it
    })

    binding.viewModel = viewModel
  }
  // endregion
}